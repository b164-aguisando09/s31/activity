
//setup dependencies
const express = require('express');
const mongoose = require('mongoose');
//This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute');

//setup server

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//database connection
mongoose.connect("mongodb+srv://orlaguisando09:Cloudorl09@orl.cxxn3.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	} 

);


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))

//Routes
app.use("/tasks", taskRoute);






app.listen(port, () => console.log(`Now listening to port ${port}`));