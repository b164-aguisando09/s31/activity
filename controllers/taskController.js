const Task = require('../models/task');

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then(	(task, error) => {
		if (error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if (err) {
			console.log(err);
			return false;
		} else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;

		}

		result.name = newContent.name;

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else{
				return updatedTask;
			}
		})
	})
}


module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(err);
			return false;
		} else{
			return result;
		}
	})
}



module.exports.updateTasktoComplete = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if (error) {
			console.log(error);
			return false;

		}

		if (newStatus === 'complete') {
			result.status = newStatus;
		} else if (newStatus === 'pending') {
			result.status = newStatus;
		} else {
			result.status = 'Wrong status. Please insert a valid status';
		}

		return result.save().then((updatedTask, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return false;
			} else{
				return updatedTask;
			}
		})
	})
}