const express = require('express');

const router = express.Router();

const TaskController = require('../controllers/taskController');

router.get("/", (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController));


});


//Creating a task

router.post("/", (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})

//delete a task
//URL "http://localhost:3001/tasks/:id"
// : - wildcard
//params parameter
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));

})

//update a task

router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


router.get("/:id", (req, res) => {
	TaskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(resultFromController));


});



router.put("/:id/:status", (req, res) => {
	TaskController.updateTasktoComplete(req.params.id, req.params['status']).then(result => res.send(result));
	
})


module.exports = router;